# Project Overview

In this project you are given a web-based application that reads RSS feeds. The original developer of this application clearly saw the value in testing, they've already included [Jasmine](http://jasmine.github.io/) and even started writing their first test suite! Unfortunately, they decided to move on to start their own company and we're now left with an application with an incomplete test suite. That's where you come in.

### Steps To Run

Open index.html in your browser of your choice.The Test are loaded at the bottom of the page with the each specs test the functionality of the feed reader testing applicaton.Once the test are loaded the following messages with green in color indicates the test passed and the red color indicates the failing of the test case at the desired location.

## List of things tested in feed reader testing
### RSS FEEDS
	Validation of allFeed Objects,its properties(name and urls.)	
	1. A feedObject is tested by expecting it to be defined and length greater than 0.
	2. All the objects of allFeeds URL are tested by expecting the URL property to be defined and length should be greater than 0. 
	3. All the objects of allFeeds object name are tested by expecting the name not to defined and length should be greater than 0. 

	The menu 	
	1. Menu item is hidden by default by checking the menu-hidden class on the body tag can pass these test or by looking the position of menu item is at -192 with respect to browser which depicts the element is hidden by default.
    2. Toggling of menu-hidden class on the body will help in test the menu-item testing pass.Initial triggerin the hamburger-icon unhides the menu-item , removes the menu-hidden class and again triggering it will add the menu-hidden class on body tag.

	Initial Entries
	1. After the loadFeed function completed execution the 10 articles are loaded into the feed container of body tag and so checking atleast one feed entries will pass the test.

	New Feed Selection 
	1. Here the old and new feeds are checked by passing into the loadFeed function and based on the comparsion we can check the feeds are loading properly.      

You can check the feedReaderTesting application by clicking the link(https://biradarsidduram.gitlab.io/fendfeedreadertesting)	