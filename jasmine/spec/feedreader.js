$(function() {
    describe('RSS Feeds', function(){
        it('all feeds are defined', function() {
            expect(allFeeds).toBeDefined();
            expect(allFeeds.length).not.toBe(0);
        });

        it("allFeeds URL'S are defined and not be empty",function(){
            allFeeds.forEach(function(feeds){
                expect(feeds.url).toBeDefined;
                expect(feeds.url.length).not.toBe(0);
            });
        });
        it("allFeeds has a name and non empty",function(){
            allFeeds.forEach(function(feeds){
                expect(feeds.name).toBeDefined;
                expect(feeds.name.length).not.toBe(0);
            });
        });
    });

    describe('The menu',function(){ 
        it('Menu is hidden by default',function(){
            // we can do these in two way.
                // 1) we can check the position of menu if less than zero and its length.
                // 2) by checking the body attr has a class name .menu-hidden.
            // expect(elem.getBoundingClientRect().x).toBe(-192);
            expect($('body').hasClass('menu-hidden')).toBe(true);
        });
        it('menu item hide and unhide upon clicking hamburger icon',function(){
            // trigger the event on icon-list and check the availability of a menu-hidden class on body
            // After first trigger hide the element and there will no menu-hidden class.On second trigger will toggle back.
            $('.icon-list').trigger('click');
            expect($('body').hasClass('menu-hidden')).toBe(false);
            $('.icon-list').trigger('click');
            expect($('body').hasClass('menu-hidden')).toBe(true);
        });
    });

    describe('Initial Entries',function(){
        beforeEach(function(done){
            loadFeed(0,function(){
                done();
            });
        });
        it('check for availability of initial entries',function(){
            // after loading atleast there must be one feed 
            expect($('.feed .entry').length).toBeGreaterThan(0);
        });
    });

    /* TODO: Write a new test suite named "New Feed Selection" */
    describe('New Feed Selection',function(){
        var oldArticle,newArticle;
        beforeEach(function(done){
            loadFeed(0,function(){
                oldArticle = $('.feed .entry')[0];
                loadFeed(1,function(){
                    newArticle = $('.feed .entry')[0];
                    done();
                });
            });
        });

        it('check for availability of new feeds',function(){
            expect(oldArticle).not.toBe(newArticle);
            console.log(oldArticle);
            console.log(newArticle);
        });
    });
}());
